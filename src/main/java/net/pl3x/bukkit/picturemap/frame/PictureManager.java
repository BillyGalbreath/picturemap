package net.pl3x.bukkit.picturemap.frame;

import net.pl3x.bukkit.picturemap.PictureMap;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import java.io.File;
import java.util.Collection;
import java.util.HashMap;

public class PictureManager {
    private PictureMap plugin;
    private HashMap<Short, Picture> pictures = new HashMap<>();

    public PictureManager(PictureMap plugin) {
        this.plugin = plugin;
    }

    public Picture createPicture(String path) {
        Picture picture = new Picture(plugin, path);
        pictures.put(picture.getMapId(), picture);
        picture.update();
        return picture;
    }

    public Picture getPicture(String path) {
        return pictures.values().stream()
                .filter(p -> p.getImagePath().equals(path))
                .findFirst().orElse(null);
    }

    public Collection<Picture> getPictures() {
        return pictures.values();
    }

    public void sendAllMaps(Player player) {
        getPictures().forEach(picture -> sendMap(picture, player));
    }

    public void sendMap(Picture picture) {
        Bukkit.getOnlinePlayers().forEach(player -> sendMap(picture, player));
    }

    public void sendMap(Picture picture, Player player) {
        if (picture.getMapView().getRenderers().isEmpty()) {
            return;
        }
        player.sendMap(picture.getMapView());
    }

    public void loadPictures() {
        pictures.clear();
        File dir = new File(plugin.getDataFolder(), "images");
        if (!dir.exists() || !dir.isDirectory()) {
            return;
        }
        File[] images = dir.listFiles((dir1, name) -> name.endsWith(".png"));
        if (images == null) {
            return; // empty directory
        }
        for (File image : images) {
            try {
                Picture picture = new Picture(plugin, image.getAbsolutePath(),
                        Short.parseShort(image.getName().split(".png")[0]));
                pictures.put(picture.getMapId(), picture);
                picture.update();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}