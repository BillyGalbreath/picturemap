package net.pl3x.bukkit.picturemap.frame;

import net.pl3x.bukkit.picturemap.PictureMap;
import net.pl3x.bukkit.picturemap.renderer.ImageRenderer;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;
import org.bukkit.map.MapRenderer;
import org.bukkit.map.MapView;

import javax.imageio.ImageIO;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.File;
import java.net.URL;

public class Picture {
    private final PictureMap plugin;
    private MapView mapView;
    private String imagePath;

    public Picture(PictureMap plugin, String imagePath) {
        this.plugin = plugin;
        this.imagePath = imagePath;
        this.mapView = Bukkit.createMap(Bukkit.getWorlds().get(0));
    }

    public Picture(PictureMap plugin, String imagePath, short id) {
        this.plugin = plugin;
        this.imagePath = imagePath;
        this.mapView = Bukkit.getMap(id);
    }

    public MapView getMapView() {
        return mapView;
    }

    public Short getMapId() {
        return mapView.getId();
    }

    public String getImagePath() {
        return imagePath;
    }

    public ItemStack getMapItem() {
        return new ItemStack(Material.MAP, 1, getMapId());
    }

    public Image getImage() {
        // get image from the web
        BufferedImage image = null;
        if ((imagePath.startsWith("http://")) || (imagePath.startsWith("https://")) || (imagePath.startsWith("ftp://"))) {
            try {
                URL url = new URL(imagePath);
                image = ImageIO.read(url);
                File dir = new File(plugin.getDataFolder(), "images");
                dir.mkdirs();
                File file = new File(dir, getMapId() + ".png");
                ImageIO.write(image, "png", file);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        // get image from the server hard drive
        else {
            File file = new File(imagePath);
            try {
                if (file.exists()) {
                    image = ImageIO.read(file);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return image == null ? null : image.getScaledInstance(128, 128, 1);
    }

    public void update() {
        for (MapRenderer render : mapView.getRenderers()) {
            mapView.removeRenderer(render);
        }
        MapView view2 = mapView;
        Image image = getImage();
        if (image == null) {
            plugin.getLogger().warning("The path \"" + getImagePath() + "\" from Frame #" + getMapId() + " does not exists!");
            return;
        }
        MapRenderer renderer = new ImageRenderer(image);
        view2.addRenderer(renderer);
        plugin.getPictureManager().sendMap(this);
    }
}
