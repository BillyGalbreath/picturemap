package net.pl3x.bukkit.picturemap;

import net.pl3x.bukkit.picturemap.commands.CmdPictureMap;
import net.pl3x.bukkit.picturemap.frame.PictureManager;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.plugin.java.JavaPlugin;

public class PictureMap extends JavaPlugin implements Listener {
    private PictureManager pictureManager;

    public void onEnable() {
        pictureManager = new PictureManager(this);
        pictureManager.loadPictures();

        getServer().getPluginManager().registerEvents(this, this);

        getCommand("picturemap").setExecutor(new CmdPictureMap(this));

        getLogger().info(getName() + " v" + getDescription().getVersion() + " enabled!");
    }

    public void onDisable() {
        getLogger().info(getName() + " disabled.");
    }

    public PictureManager getPictureManager() {
        return pictureManager;
    }

    @EventHandler(priority = EventPriority.NORMAL)
    public void onPlayerJoin(PlayerJoinEvent event) {
        pictureManager.sendAllMaps(event.getPlayer());
    }
}
