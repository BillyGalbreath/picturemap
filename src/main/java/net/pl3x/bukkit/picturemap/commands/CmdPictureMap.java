package net.pl3x.bukkit.picturemap.commands;

import net.pl3x.bukkit.picturemap.PictureMap;
import net.pl3x.bukkit.picturemap.frame.Picture;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.command.TabExecutor;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class CmdPictureMap implements TabExecutor {
    private PictureMap plugin;

    public CmdPictureMap(PictureMap plugin) {
        this.plugin = plugin;
    }

    @Override
    public List<String> onTabComplete(CommandSender sender, Command command, String label, String[] args) {
        return new ArrayList<>();
    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if (!(sender instanceof Player)) {
            sender.sendMessage(ChatColor.RED + "This command is only available to players");
            return true;
        }

        if (!sender.hasPermission("picturemap.create")) {
            sender.sendMessage(ChatColor.RED + "You do not have permission for that command!");
            return true;
        }

        if (args.length == 0) {
            sender.sendMessage(ChatColor.RED + "Please specify URL");
            return false;
        }

        Picture picture = plugin.getPictureManager().getPicture(args[0]);
        if (picture != null) {
            ItemStack item = picture.getMapItem();
            if (item == null) {
                sender.sendMessage(ChatColor.RED + "Something went wrong");
                return true;
            }

            HashMap<Integer, ItemStack> leftover = ((Player) sender).getInventory().addItem(item);
            if (!leftover.isEmpty()) {
                sender.sendMessage(ChatColor.RED + "Your inventory seems to be full");
                return true;
            }
            sender.sendMessage(ChatColor.LIGHT_PURPLE + "Image created");
            return true;
        }

        picture = plugin.getPictureManager().createPicture(args[0]);
        if (picture == null) {
            sender.sendMessage(ChatColor.RED + "Something went wrong");
            return true;
        }

        ItemStack item = picture.getMapItem();
        if (item == null) {
            sender.sendMessage(ChatColor.RED + "Something went wrong");
            return true;
        }

        HashMap<Integer, ItemStack> leftover = ((Player) sender).getInventory().addItem(item);
        if (!leftover.isEmpty()) {
            sender.sendMessage(ChatColor.RED + "Your inventory seems to be full");
            return true;
        }
        sender.sendMessage(ChatColor.LIGHT_PURPLE + "Image created");
        return true;
    }
}
